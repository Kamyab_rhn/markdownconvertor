﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkdownConverter
{
    static class In_Line
    {
        public static bool SetTag(ref List<string> lines, int lineIndex, int startCharIndex, char character, string tag)
        {
            string sub = lines[lineIndex].Substring(startCharIndex + 1);
            if (sub.Contains(character.ToString()))
            {
                lines[lineIndex] = lines[lineIndex].Remove(startCharIndex, 1);
                lines[lineIndex] = lines[lineIndex].Insert(startCharIndex, "<" + tag + ">");
                int endIndex = sub.IndexOf(character.ToString(), StringComparison.Ordinal) + startCharIndex +
                               tag.Length + 2;

                lines[lineIndex] = lines[lineIndex].Remove(endIndex, 1);
                lines[lineIndex] = lines[lineIndex].Insert(endIndex, "</" + tag + ">");
                return true;
            }
            return false;
        }
        public static bool SetTag(ref List<string> lines, int lineIndex, int startCharIndex, string characters, string tag)
        {
            string sub = lines[lineIndex].Substring(startCharIndex + 2);
            if (sub.Contains(characters))
            {
                lines[lineIndex] = lines[lineIndex].Remove(startCharIndex, 2);
                lines[lineIndex] = lines[lineIndex].Insert(startCharIndex, "<" + tag + ">");
                int endIndex = sub.IndexOf(characters, StringComparison.Ordinal) + startCharIndex +
                               tag.Length + 2;

                lines[lineIndex] = lines[lineIndex].Remove(endIndex, 2);
                lines[lineIndex] = lines[lineIndex].Insert(endIndex, "</" + tag + ">");
                return true;
            }
            return false;
        }
    }
}
