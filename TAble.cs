﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MarkdownConverter
{
    static class Table
    {
        public static bool SetTag(ref List<string> lines, int index)
        {
            List<int> tableLineIndexes = new List<int> {index};
            int charCount = lines[index].Count(x => x == '|');
            for (int j = index + 1; j < lines.Count; j++)
            {
                if (lines[j].Contains('|') && lines[j].Count(x => x == '|') == charCount)
                {
                    tableLineIndexes.Add(j);
                }
                else
                {
                    break;
                }
            }
            foreach (int idx in tableLineIndexes)
            {
                if (lines[idx].StartsWith("|") && lines[idx].EndsWith("|"))
                {
                    lines[idx] = lines[idx].Remove(0, 1);
                    lines[idx] = lines[idx].Remove(lines[idx].Length - 1, 1);
                }
            }
            if (tableLineIndexes.Count < 2)
                return false;

            string[] colsAlign = lines[tableLineIndexes[1]].Split('|');
            int colsCount = colsAlign.Length;
            for (int x = 0; x < colsCount; x++)
            {
                Regex left1 = new Regex(@"^:-{3,}$");
                Regex left2 = new Regex(@"^-{3,}$");
                Regex right = new Regex(@"^-{3,}:$");
                Regex center = new Regex(@"^:-{3,}:$");

                if (left1.IsMatch(colsAlign[x].Trim()) || left2.IsMatch(colsAlign[x].Trim()))
                {
                    colsAlign[x] = "left";
                }
                else if (right.IsMatch(colsAlign[x].Trim()))
                {
                    colsAlign[x] = "right";
                }
                else if (center.IsMatch(colsAlign[x].Trim()))
                {
                    colsAlign[x] = "center";
                }
                else
                {
                    return false;
                }
            }

            tableLineIndexes.RemoveAt(1);
            for (int x = 0; x < tableLineIndexes.Count; x++)
            {
                string[] cols = lines[tableLineIndexes[x]].Split('|');
                string result = "";
                if (x == 0)
                {
                    result += "</p><table border=\"1\">";
                    result += "<tr>";
                    for (int y = 0; y < cols.Length; y++)
                    {
                        result += "<th align = \"" + colsAlign[y] + "\">";
                        result += cols[y].Trim() + "</th>";
                    }
                }
                else
                {
                    result += "<tr>";
                    for (int y = 0; y < cols.Length; y++)
                    {
                        result += "<td align = \"" + colsAlign[y] + "\">";
                        result += cols[y].Trim() + "</td>";
                    }
                }
                lines[tableLineIndexes[x]] = result;
            }
            lines[tableLineIndexes[tableLineIndexes.Count - 1]] += "</table><p>";
            lines.RemoveAt(tableLineIndexes[0] + 1); //remove alignment row in the file
            return true;
        }
    }
}
