﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkdownConverter
{
    static class Hashtag
    {
        public static void SetHeader(ref List<string> lines, int index, char[] chars)
        {
            List<int> indexes = new List<int>();
            int count = 0;
            for (int ch = 0; ch < chars.Length; ch++)
            {
                if (chars[ch] == '#')
                    indexes.Add(ch);
            }
            for (int a = 0; a < indexes.Count; a++)
            {
                if (indexes[a] == a)
                {
                    count++;
                }
                else if (indexes[a] != a)
                    break;
            }
            lines[index] = lines[index].Remove(0, count);
            lines[index] = lines[index].TrimStart();
            if (count > 0)
                lines[index] = "</p><h" + count + ">" + lines[index] + "</h" + count + "><p>";
        }
    }
}
