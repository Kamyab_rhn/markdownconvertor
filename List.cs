﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace MarkdownConverter
{
    public class List
    {
        private List<char> _ignoringChars;
        public List()
        {
            _ignoringChars = new List<char>();
        }
       
        public bool SetUnOrderedListTag(ref List<string> lines, int lineIndex, char character, out int numberOfItems, bool isNested = false)
        {
            numberOfItems = 0;
            _ignoringChars.Add(character);
            string trimString = lines[lineIndex].Trim();
            if (trimString.StartsWith(character.ToString()) && !trimString.EndsWith(character.ToString()))
            {
                List<int> uListLineIndexes = new List<int> {lineIndex};
                lines[lineIndex] = lines[lineIndex].Remove(0, 1);
                for (int j = lineIndex + 1; j < lines.Count; j++)
                {
                    trimString = lines[j].Trim();
                    if (trimString.StartsWith(character.ToString()) && !trimString.EndsWith(character.ToString()))
                    {
                        lines[j] = lines[j].Remove(0, 1);
                        uListLineIndexes.Add(j);
                    }
                    else if (CreateNestedList(ref lines, j, out int itemsCount))
                    {
                        j += itemsCount - 1;
                        numberOfItems += itemsCount;
                    }
                    else
                        break;
                }
                lines[uListLineIndexes[0]] = "<ul><li>" + lines[uListLineIndexes[0]];
                if (uListLineIndexes.Count > 1)
                    for (int i = uListLineIndexes[1]; i <= uListLineIndexes[uListLineIndexes.Count - 1]; i++)
                    {
                        if (!lines[i].StartsWith("<"))
                        {
                            lines[i] = "</li><li>" + lines[i];
                        }
                    }
                lines[uListLineIndexes[uListLineIndexes.Count - 1]] += "</li></ul>";

                if (!isNested)
                {
                    lines[uListLineIndexes[0]] = "</p>" + lines[uListLineIndexes[0]];
                    lines[uListLineIndexes[uListLineIndexes.Count - 1]] += "<p>";
                }
                numberOfItems += uListLineIndexes.Count;
                return true;
            }
            numberOfItems = 0;
            return false;
        }

        public bool SetOrderedListTag(ref List<string> lines, int index, out int numberOfItems, bool isNested = false)
        {
            numberOfItems = 0;
            if (lines[index]?.Substring(0, 2) == "1.")
            {
                List<int> listLineIndexes = new List<int> {index};
                lines[index] = lines[index].Remove(0, 2);
                for (int j = index + 1, counter = 2; j < lines.Count; j++, counter++)
                {
                    if (lines[j].StartsWith(counter + "."))
                    {
                        lines[j] = lines[j].Remove(0, 2);
                        listLineIndexes.Add(j);

                        if (!isNested)
                            _ignoringChars.Clear();
                    }
                    else if (CreateNestedList(ref lines, j, out int itemsCount))
                    {
                        counter--;
                        j += itemsCount - 1;
                        numberOfItems += itemsCount;
                    }
                    else
                        break;
                }
               
                lines[listLineIndexes[0]] = "<ol><li>" + lines[listLineIndexes[0]];

                if(listLineIndexes.Count > 1)
                    for (int i = listLineIndexes[1]; i <= listLineIndexes[listLineIndexes.Count - 1]; i++)
                    {
                        if (!lines[i].StartsWith("<"))
                        {
                            lines[i] = "</li><li>" + lines[i];
                        }
                    }

                lines[listLineIndexes[listLineIndexes.Count - 1]] += "</li></ol>";

                if (!isNested)
                {
                    lines[listLineIndexes[0]] = "</p>" + lines[listLineIndexes[0]];
                    lines[listLineIndexes[listLineIndexes.Count - 1]] += "<p>";
                }
                numberOfItems += listLineIndexes.Count;
                return true;
            }
            numberOfItems = 0;
            return false;
        }

        public bool CreateNestedList(ref List<string> lines, int index, out int itemsCount)
        {
            if (lines[index].StartsWith("*") && !_ignoringChars.Contains('*'))
            {
                SetUnOrderedListTag(ref lines, index, '*', out itemsCount, true);
            }
            else if (lines[index].StartsWith("-") && !_ignoringChars.Contains('-'))
            {
                SetUnOrderedListTag(ref lines, index, '-', out itemsCount, true);
            }
            else if (lines[index].StartsWith("+") && !_ignoringChars.Contains('+'))
            {
                SetUnOrderedListTag(ref lines, index, '+', out itemsCount, true);
            }
            else if (lines[index].StartsWith("1."))
            {
                SetOrderedListTag(ref lines, index, out itemsCount, true);
            }
            else
            {
                itemsCount = 0;
                return false;
            }
            return true;
        }
    }
}
