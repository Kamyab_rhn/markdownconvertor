﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MarkdownConverter
{
    class Processor
    {
        private List<string> _lines;
        private string _path;
        private readonly Dictionary<string, string> _references;
        public Processor(string path)
        {
            _path = path;
            _lines = new List<string>();
            _references = new Dictionary<string, string>();
        }
		
		 public void StoreHtmlFile(string title, string description, string author)
        {
            List<string> htmlLines = new List<string>
            {
                "<!DOCTYPE html>",
                "<html lang=\"en\">",
                "<head>",
                "<meta charset=\"utf-8\" />",
                "<meta name=\"description\" content=\"" + description + "\"/>",
                "<meta name=\"keywords\" content=\"\" />",
                "<meta name=\"author\" content=\"" + author + "\"/>",
                "<title>" + title + "</title>",
                "</head>",
                "<body>",
                "<div>",
                "<p>"
            };
            htmlLines.AddRange(_lines);
            htmlLines.AddRange(new List<string>
            {
                "</p>",
                "</div>",
                "</body>",
                "</html>"
            });

            FileStream fs = new FileStream(Path.GetPathRoot(_path) + Path.GetFileNameWithoutExtension(_path) + ".html",
                FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);
            foreach (string l in htmlLines)
            {
                if(!l.TrimStart().StartsWith("<"))
                    sw.WriteLine("<br/>");

                sw.WriteLine(l);
            }
            sw.Flush();
            sw.Close();
            fs.Close();
        }
		
        public void FileToList()
        {
            FileStream fs = new FileStream(_path, FileMode.Open);
            StreamReader sr = new StreamReader(fs);

            string line;
            while ((line = sr.ReadLine()) != null)
            {
                _lines.Add(line);
            }

            sr.Dispose();
            sr.Close();
            fs.Close();
        }

        public void GetReferences()
        {
            List<string> toBeRemoved = new List<string>();
            foreach (string l in _lines)
            {
                if (l.Contains("]:"))
                {
                    toBeRemoved.Add(l);
                    string[] keyValue = l.Trim().Split(new char[] {':'}, 2);
                    _references[keyValue[0].ToLower()] = keyValue[1].ToLower().Trim();
                }
            }
            foreach (string t in toBeRemoved)
            {
                _lines.Remove(t);
            }
        }
        public bool ExamineLines() //true if special char is found
        {
            for (int i = 0; i < _lines.Count; i++)
            {
                _lines[i] = _lines[i].TrimStart();
                char[] chars = _lines[i].ToCharArray();
                bool specialCharFound = false;
                if (chars.Length == 0)
                {
                    _lines[i] = "</p><p>";
                }
                for (int c = 0; c < chars.Length; c++)
                {
                    switch (chars[c])
                    {
                        case '#':
                            Hashtag.SetHeader(ref _lines, i, chars);
                            specialCharFound = true;
                            break;
                        case '*':
                            specialCharFound = true;
                            Regex hrStar = new Regex(@"\*{3,}");
                            if (hrStar.IsMatch(_lines[i]))
                                _lines[i] = "<hr/>";

                            else if (chars[c + 1] == '*' && In_Line.SetTag(ref _lines, i, c, "**", "strong")) ;
                            else if (new List().SetUnOrderedListTag(ref _lines, i, '*', out _)) ;
                            else if (In_Line.SetTag(ref _lines, i, c, '*', "em")) ;
                            else
                                specialCharFound = false;
                            break;
                        case '_':
                            specialCharFound = true;

                            Regex hrUnderScore = new Regex(@"_{3,}");
                            if (hrUnderScore.IsMatch(_lines[i]))
                                _lines[i] = "<hr/>";

                            else if (chars[c + 1] == '_' && In_Line.SetTag(ref _lines, i, c, "__", "strong")) ;
                            else if (In_Line.SetTag(ref _lines, i, c, '_', "em")) ;
                            else
                                specialCharFound = false;
                            break;
                        case '`':
                            if (_lines[i]?.TrimStart().Substring(0, 3) == "```")
                            {
                                for (int j = i + 1; j < _lines.Count; j++)
                                {
                                    if (_lines[j].TrimStart().Substring(0, 3) == "```")
                                    {
                                        _lines[i] = "</p><code>";
                                        _lines[j] = "</code><p>";
                                        specialCharFound = true;
                                        break;
                                    }
                                }
                            }
                            else if (In_Line.SetTag(ref _lines, i, c, '`', "mark"))
                                specialCharFound = true;

                            break;
                        case '~':
                            if(In_Line.SetTag(ref _lines, i, c, '~', "strike"))
                                specialCharFound = true;
                            break;
                        case '-':
                            Regex hrDash = new Regex(@"-{3,}");
                            if (hrDash.IsMatch(_lines[i]))
                            {
                                _lines[i] = "<hr/>";
                                specialCharFound = true;
                                break;
                            }

                            if(new List().SetUnOrderedListTag(ref _lines, i, '-', out _))
                                specialCharFound = true;
                            break;
                        case '+':
                            if(new List().SetUnOrderedListTag(ref _lines, i, '+', out _))
                                specialCharFound = true;
                            break;
                        case '[':
                            if (SquareBracketElements.SetTag(ref _lines, i, _references))
                                specialCharFound = true;
                            break;
                        case '!':
                            if (SquareBracketElements.SetTag(ref _lines, i, _references, true))
                                specialCharFound = true;
                            break;
                        case '|':
                            if (Table.SetTag(ref _lines, i))
                                specialCharFound = true;
                            break;
                        case '>':
                            int endLineIndex = i;
                            if (_lines[i].TrimStart()[0] == '>')
                            {
                                _lines[i] = _lines[i].Remove(_lines[i].IndexOf('>'), 1);
                                _lines[i] = _lines[i].TrimStart().Insert(0, "</p><blockquote>");
                                for (int j = i + 1; j < _lines.Count; j++)
                                {
                                    if (_lines[j].ToCharArray().Length == 0 || _lines[j].TrimStart()[0] != '>')
                                    {
                                        endLineIndex = j - 1;
                                        break;
                                    }
                                    _lines[j] = _lines[j].Remove(_lines[j].IndexOf('>'), 1);
                                    _lines[j] = _lines[j].TrimStart();
                                }
                                _lines[endLineIndex] = _lines[endLineIndex] + "</blockquote><p>";
                                specialCharFound = true;
                            }
                            break;
                        case '1':
                            if(new List().SetOrderedListTag(ref _lines, i, out _))
                                specialCharFound = true;
                            break;
                        default:
                            if (_lines[i].Trim() == string.Empty || _lines[i] == null)
                            {
                                _lines[i] = "</p><p>";
                            }
                            specialCharFound = false;
                            break;
                    }

                    if (specialCharFound)
                        return true;
                }
            }
            return false;
        }

        public List<string> Lines => _lines;
    }
}
