﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkdownConverter
{
    static class SquareBracketElements
    {
        public static bool SetTag(ref List<string> lines, int index, Dictionary<string, string> references, bool isImage = false)
        {
            string toRemove;
            if (isImage)
                lines[index] = lines[index].Replace("!", "");
            int closingBracketIndex = GetRightClosingBracketIndex(lines[index], lines[index].IndexOf('['));
            string text = lines[index].Substring(lines[index].IndexOf('[') + 1,
                closingBracketIndex - (lines[index].IndexOf('[') + 1));

            string secondPart = lines[index].Substring(closingBracketIndex + 1);
            string href = "";
            string title = "";
            if (secondPart.StartsWith("["))
            {
                secondPart = secondPart.Substring(0, secondPart.IndexOf(']') + 1);
                href = references[secondPart.ToLower()].Split('"')[0].Trim();
                if (references[secondPart.ToLower()].Split('"').Length > 1)
                    title = references[secondPart.ToLower()].Split('"')[1]?.Trim();

                toRemove = "[" + text + "]" + secondPart;
            }
            else if (secondPart.StartsWith("("))
            {
                int closingBracketIndex2 = GetRightClosingBracketIndex(secondPart, secondPart.IndexOf('('));
                secondPart = secondPart.Substring(0, closingBracketIndex2 + 1);
                string sub = secondPart.Substring(secondPart.IndexOf('(') + 1,
                    closingBracketIndex2 - (secondPart.IndexOf('(') + 1));
                href = sub.Split('"')[0].Trim();
                if (sub.Split('"').Length > 1)
                    title = sub.Split('"')[1].Trim();

                toRemove = "[" + text + "]" + secondPart;
            }
            else if(references.ContainsKey("[" + text.ToLower() + "]"))
            {
                href = references["[" + text.ToLower() + "]"].Split('"')[0].Trim();
                if (references["[" + text.ToLower() + "]"].Split('"').Length > 1)
                    title = references["[" + text.ToLower() + "]"].Split('"')[1]?.Trim();

                toRemove = "[" + text + "]";
            }
            else
            {
                return false;
            }
            if (isImage)
            {
                lines[index] = lines[index].Replace(toRemove, $"</p><img src=\"{href}\" alt=\"{text}\" title=\"{title}\"><p>");
            }
            else
            {
                lines[index] = lines[index].Replace(toRemove, $"<a href=\"{href}\" title=\"{title}\">{text}</a>");
            }
            return true;
        }

        public static int GetRightClosingBracketIndex(string line, int openBracketIndex)
        {
            char closeBracket = line[openBracketIndex] == '[' ? ']' : ')';

            List<int> closeBracketIndexes = AllIndexesOf(line, closeBracket);

            for (int i = closeBracketIndexes.Count - 1; i >= 0; i--)
            {
                foreach (int t in closeBracketIndexes)
                {
                    if (t < closeBracketIndexes[i] && t > openBracketIndex)
                    {
                        break;
                    }
                    if (!line.Substring(openBracketIndex + 1, closeBracketIndexes[i] - (openBracketIndex + 1))
                        .Contains(line[openBracketIndex]))
                        return closeBracketIndexes[i];

                    return closeBracketIndexes[i + 1];
                }
            }
            return -1;
        }

        public static List<int> AllIndexesOf(string str, char character)
        {
            List<int> result = new List<int>();
            for (int i = str.IndexOf(character); i > -1; i = str.IndexOf(character, i + 1))
            {
                result.Add(i);
            }
            return result;
        }
    }
}
