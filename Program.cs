﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkdownConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Title:");
            string title = Console.ReadLine();
            Console.Write("Description:");
            string description = Console.ReadLine();
            Console.Write("Author:");
            string author = Console.ReadLine();
            Console.Write("Path (markdown file):");
            string path = Console.ReadLine();
            Console.WriteLine("Conveerting ...");
            Processor myProcessor = new Processor(path);
            myProcessor.FileToList();
            myProcessor.GetReferences();
            bool recursive;
            do
            {
                recursive = myProcessor.ExamineLines();
            } while (recursive);
            myProcessor.StoreHtmlFile(title, description, author);
            Console.WriteLine("Succesful!!\nPress Enter to continue ...");
            Console.ReadLine();
        }
    }
}
